from django.urls import path

app_name = 'clientes'

from . import views

urlpatterns = [
    path('clientes/', views.ClienteListCreateAPIView.as_view(), name='create_list_clientes'),
]